﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bidding.Model
{
    public class Product
    {
        public List<Auction> Auctions { get; set; }
    }
}
