﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bidding.Model
{
    public class Bid
    {
        public decimal OfferPrice { get; set; }
        public int BuyerId { get; set; }
    }
}
