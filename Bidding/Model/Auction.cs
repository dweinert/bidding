﻿using System;
using System.Collections.Generic;

namespace Bidding.Model
{
    public class Auction
    {
        public Auction(int id, decimal askingPrice, string title, int durationInDays)
        {
            Id = id;
            AskingPrice = askingPrice;
            Title = title;
            EndDate = DateTime.UtcNow.AddDays(durationInDays);
        }

        public int Id { get; set; }
        public decimal AskingPrice { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EndDate { get; set; }
        public List<Bid> Bids { get; set; }
    }
}
