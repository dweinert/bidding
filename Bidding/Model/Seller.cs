﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bidding.Model
{
    public class Seller
    {
        public List<Product> Products { get; set; }
    }
}
