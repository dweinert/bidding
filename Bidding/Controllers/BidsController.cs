using System.Collections.Generic;
using System.Threading.Tasks;
using Bidding.Model;
using Bidding.Repos;
using Microsoft.AspNetCore.Mvc;

namespace Bidding.Controllers
{
    [Route("api/[controller]")]
    public class BidsController : Controller
    {
        private readonly IBidRepository _repository;

        public BidsController(IBidRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("[action]")]
        [ProducesResponseType(200, Type = typeof(Bid))]
        public async Task<IEnumerable<Bid>> Bids(int auctionId)
        {
            return await _repository.GetBidsAsync(auctionId);
        }
    }
}
