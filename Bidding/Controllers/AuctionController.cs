using System.Collections.Generic;
using System.Threading.Tasks;
using Bidding.Model;
using Bidding.Repos;
using Microsoft.AspNetCore.Mvc;

namespace Bidding.Controllers
{
    [Route("api/[controller]")]
    public class AuctionController : Controller
    {
        private readonly IAuctionRepository _repository;

        public AuctionController(IAuctionRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(Auction))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var auction = await _repository.GetAuctionAsync(id);
            if (auction == null)
                return NotFound();

            return Ok(auction);
        }

        [HttpGet("")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Auction>))]
        public async Task<IActionResult> Get()
        {
            return Ok(await _repository.GetAuctionsAsync());
        }
    }
}
