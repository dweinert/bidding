import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/auctionDetailReducer';
import moment from 'moment';

class AuctionDetail extends Component {
    componentWillMount() {
        const auctionId = parseInt(this.props.match.params.auctionId, 10) || 0;
        this.props.requestAuctionDetail(auctionId);
    }

    render() {
        return (
            <div>
                <h1>Auction Detail</h1>
                <table className='table'>
                    <tbody>
                        <tr>
                            <td>Title: </td>
                            <td>{this.props.auctionDetail.title}</td>
                        </tr>
                        <tr>
                            <td>Asking Price: </td>
                            <td>{this.props.auctionDetail.askingPrice}</td>
                        </tr>
                        <tr>
                            <td>End Date: </td>
                            <td>{moment(this.props.auctionDetail.endDate).format('MMMM Do YYYY, h:mm:ss A')}</td>
                        </tr>
                        <tr>
                            <td>Status: </td>
                            <td>{Date.now() < Date.parse(this.props.auctionDetail.endDate) ? 'Open' : 'Closed'}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default connect(
    state => state.auctionDetail,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(AuctionDetail);
