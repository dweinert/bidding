import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/auctionListReducer';
import moment from 'moment';

class OpenAuctions extends Component {
    componentWillMount() {
        const startIndex = parseInt(this.props.match.params.startIndex, 10) || 0;
        this.props.requestAuctions(startIndex);
    }

    render() {
        return (
            <div>
                <h1>Open Auctions</h1>
                <p>Below is a list of auctions still open for bidding.</p>
                {renderAuctionsTable(this.props)}
                {renderPagination(this.props)}
            </div>
        );
    }
}

function renderAuctionsTable(props) {
    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Asking Price</th>
                    <th>End Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {props.auctionList.map(auction =>
                    <tr key={auction.id}>
                        <td>{auction.title}</td>
                        <td>{auction.askingPrice}</td>
                        <td>{moment(auction.endDate).format('MMMM Do YYYY, h:mm:ss A')}</td>
                        <td><Link to={`/auctiondetail/${auction.id}`}>Place Bid</Link></td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}

function renderPagination(props) {
    return <p className='clearfix text-center'>
        <a class='btn btn-default pull-left' disabled>Previous</a>
        <a class='btn btn-default pull-right' disabled>Next</a>
        {props.isLoading ? <span>Loading...</span> : []}
    </p>;
}

export default connect(
    state => state.auctionList,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(OpenAuctions);
