import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/auctionListReducer';
import moment from 'moment';

class ClosedAuctions extends Component {
    componentWillMount() {
        // This method runs when the component is first added to the page
        const startIndex = parseInt(this.props.match.params.startIndex, 10) || 0;
        this.props.requestAuctions(startIndex);
    }

    componentWillReceiveProps(nextProps) {
        // This method runs when incoming props (e.g., route params) change
        const startIndex = parseInt(nextProps.match.params.startIndex, 10) || 0;
        this.props.requestAuctions(startIndex);
    }

    render() {
        return (
            <div>
                <h1>Closed Auctions</h1>
                <p>Below is a list of auctions that have recently closed. They are listed in decending order of auction date.</p>
                {renderAuctionsTable(this.props)}
                {renderPagination(this.props)}
            </div>
        );
    }
}

function renderAuctionsTable(props) {
    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Asking Price</th>
                    <th>End Date</th>
                </tr>
            </thead>
            <tbody>
                {props.auctionList.map(auction =>
                    <tr key={auction.id}>
                        <td>{auction.title}</td>
                        <td>{auction.askingPrice}</td>
                        <td>{moment(auction.endDate).format('MMMM Do YYYY, h:mm:ss A')}</td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}

function renderPagination(props) {
    const prevStartIndex = (props.startIndex || 0) - 5;
    const nextStartIndex = (props.startIndex || 0) + 5;

    return <p className='clearfix text-center'>
        <Link className='btn btn-default pull-left' to={`/auctionindex/${prevStartIndex}`}>Previous</Link>
        <Link className='btn btn-default pull-right' to={`/auctionindex/${nextStartIndex}`}>Next</Link>
        {props.isLoading ? <span>Loading...</span> : []}
    </p>;
}

export default connect(
    state => state.auctionList,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(ClosedAuctions);
