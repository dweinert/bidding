import React from 'react';
import { connect } from 'react-redux';

const Home = props => (
    <div>
        <h1>Bidding</h1>
        <h3>Architecture</h3>
        <ul>
            <li>MongoDB</li>
            <li>.net Core 2.1 Web API</li>
            <li>React/Redux</li>
            <li>Lambda cron job to close/archive expired auctions</li>
            <li>Unit/integration tests/automated tests</li>
        </ul>
  </div>);

export default connect()(Home);
