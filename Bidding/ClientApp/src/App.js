﻿import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import OpenAuctions from './components/OpenAuctions';
import ClosedAuctions from './components/ClosedAuctions';
import AuctionDetail from './components/AuctionDetail';

export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/openauctions' component={OpenAuctions} />
        <Route path='/closedauctions' component={ClosedAuctions} />
        <Route path='/auctiondetail/:auctionId' component={AuctionDetail} />
    </Layout>
);
