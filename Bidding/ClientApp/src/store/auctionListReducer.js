﻿const requestAuctionsType = 'REQUEST_AUCTIONS';
const receiveAuctionsType = 'RECEIVE_AUCTIONS';
const initialState = { auctionList: [], isLoading: false };

export const actionCreators = {
    requestAuctions: startIndex => async (dispatch, getState) => {
        if (startIndex === getState().auctionList.startIndex) {
            // Don't issue a duplicate request (we already have or are loading the requested data)
            return;
        }

        dispatch({ type: requestAuctionsType, startIndex });

        const url = `api/auction`;
        const response = await fetch(url);
        const auctionList = await response.json();

        dispatch({ type: receiveAuctionsType, startIndex, auctionList });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case requestAuctionsType: {
            return {
                ...state,
                startIndex: action.startIndex,
                isLoading: true
            };
        }
        case receiveAuctionsType: {
            return {
                ...state,
                startIndex: action.startIndex,
                auctionList: action.auctionList,
                isLoading: false
            };
        }
        default: {
            return state;
        }
    }
};
