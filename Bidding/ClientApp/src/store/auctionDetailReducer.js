﻿const requestAuctionDetailType = 'REQUEST_AUCTION_DETAIL';
const receiveAuctionDetailType = 'RECEIVE_AUCTION_DETAIL';
const initialState = { auctionDetail: {}, isLoading: false };

export const actionCreators = {
    requestAuctionDetail: auctionId => async (dispatch, getState) => {
        dispatch({ type: requestAuctionDetailType, auctionId });

        const url = `api/auction/${auctionId}`;
        const response = await fetch(url);
        const auctionDetail = await response.json();

        dispatch({ type: receiveAuctionDetailType, auctionDetail });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case requestAuctionDetailType: {
            return {
                ...state,
                startIndex: action.startIndex,
                isLoading: true
            };
        }
        case receiveAuctionDetailType: {
            return {
                ...state,
                startIndex: action.startIndex,
                auctionDetail: action.auctionDetail,
                isLoading: false
            };
        }
        default: {
            return state;
        }
    }
};
