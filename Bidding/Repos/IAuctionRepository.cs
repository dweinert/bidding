﻿using Bidding.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bidding.Repos
{
    public interface IAuctionRepository
    {
        Task<Auction> GetAuctionAsync(int auctionId);
        Task<IEnumerable<Auction>> GetAuctionsAsync();
    }
}
