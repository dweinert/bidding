﻿using Bidding.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bidding.Repos
{
    public interface IBidRepository
    {
        Task<IEnumerable<Bid>> GetBidsAsync(int auctionId);
    }
}
