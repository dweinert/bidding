﻿using Bidding.Database;
using Bidding.Model;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bidding.Repos
{
    public class AuctionRepository : IAuctionRepository
    {
        private readonly ILogger<AuctionRepository> _logger;
        private readonly IMongoDatabase _mongoDb;

        public AuctionRepository(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<AuctionRepository>();

            // mongoDb would be injected
            //_mongoDb = mongoDb;
        }

        public async Task<Auction> GetAuctionAsync(int auctionId)
        {
            return _mockAuctions.FirstOrDefault(a => a.Id == auctionId);
        }

        public async Task<IEnumerable<Auction>> GetAuctionsAsync()
        {
            // Normally we'd be hitting _mongoDb for data
            return _mockAuctions;
        }

        private IEnumerable<Auction> _mockAuctions = new List<Auction>
        {
            new Auction(1, 330m, "Common Projects low top sneakers - 9 US - white", 7),
            new Auction(2, 120m, "Bottle Logic - Fundamental Observation - imperial stout - 13.2% ABV", 10),
            new Auction(3, 800m, "Samsung Galaxy S8+", 5),
            new Auction(4, 13.5m, "Foam roller - 40cm", -3)
        };
    }
}
