﻿using Bidding.Database;
using Bidding.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bidding.Repos
{
    public class BidRepository : IBidRepository
    {
        private readonly ILogger<BidRepository> _logger;
        private readonly IMongoDatabase _mongoDb;

        public BidRepository(ILoggerFactory loggerFactory, IMongoDatabase mongoDb)
        {
            _logger = loggerFactory.CreateLogger<BidRepository>();
            _mongoDb = mongoDb;
        }

        public Task<IEnumerable<Bid>> GetBidsAsync(int auctionId)
        {
            throw new NotImplementedException();
        }
    }
}
